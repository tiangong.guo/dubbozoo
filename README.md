#dubbozoo
Nodejs调用Dubbo协议，在zookeeper获取服务，通过hessian协议调用Dubbo服务  
**Demo地址:**  
消费端:  
[https://git.oschina.net/qilong/hessian-consumer-nodejs-demo](https://git.oschina.net/qilong/hessian-consumer-nodejs-demo)  
服务端:  
[https://git.oschina.net/qilong/hessian-server-demo](https://git.oschina.net/qilong/hessian-server-demo)  

* 安装  
```javascript
npm install dubbozoo
```

* 使用  
```javascript
var ZD = require('dubbozoo');
var zd = new ZD({
    // config the addresses of zookeeper
    conn: '127.0.0.1:2181',
    // dubbo version
    dubbo: '2.8.4'
});
// connect to zookeeper
zd.connect();
zd.getProvider('com.ql.hessian.demo.api.HessianDemoProvider','1.0', function (err, provider) {
    if (err){
        next(err);
    } else {
        provider.invoke("getById", [12], function (err, data) {
            if (err) {
                // 处理错误
            };
            // 处理数据
            console.log(data);
            // data为服务端返回数据
        });
    }
});
```

**参数说明**  
* 获取服务  
zd.getProvider(interfaceClass, version, function(err, provider))  
interfaceClass: 服务的接口类  
version：服务版本  
function函数:   
    provider: 获取到的服务对象  

* 调用方法
provider.invoke(methodName, args, function(err, data){})  
methodName: 方法名字  
args: 参数，数组  

**欢迎交流**  
 git: [https://git.oschina.net/qilong/dubbozoo](https://git.oschina.net/qilong/dubbozoo)  
 email: [qilongjava@163.com](mailto://qilongjava@163.com)  
 QQ: [450457412](tencent://message/?uin=450457412&Menu=yes)  
 
 